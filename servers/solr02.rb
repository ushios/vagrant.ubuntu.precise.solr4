$solr02 = Proc.new{|conf|
	conf.vm.box = $boxes[:precise64]

	conf.vm.network :private_network, ip: "192.168.100.12"
	conf.vm.network :forwarded_port, guest:80, host:8082
	
	conf.vm.provision :chef_solo do |chef|
		chef.cookbooks_path = $cookbooks_path
		chef.roles_path = $roles_path
		
		chef.add_recipe 'timezone::asia_tokyo'
		chef.add_recipe 'apt::update_once'
		
		# add rolse (roles folder)		
		chef.add_role 'vim'
		chef.add_role 'solr4'

		chef.json = {
			:solr4 => {
				:port => 80,
				:host => '192.168.100.12',
				:zkHost => '192.168.100.10:2181',
				:maxMemory => '32m',
				:minMemory => '16m'
			}
		}
	end
}

